const express = require('express')
const app = express()
app.use(express.static(`${__dirname}/frontend/build`))
app.listen(8080)
